require 'ruble'

with_defaults :scope => "source.js" do 
    
#=====微信小程序/start========================


snippet 'wx.setStorage' do |s|
  s.trigger = 'wx.setStorage'
  s.description = '将数据存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个异步接口'
  s.expansion = "//将数据存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个异步接口
wx.setStorage({
  key:\"key\",
  data:\"value\"
})"
end
 

snippet 'wx.setStorageSync' do |s|
  s.trigger = 'wx.setStorageSync'
  s.description = '将 data 存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个同步接口。'
  s.expansion = "//将 data 存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个同步接口。
try {
    wx.setStorageSync('key', 'value')
} catch (e) {    
}"
end
 

snippet 'wx.getStorage' do |s|
  s.trigger = 'wx.getStorage'
  s.description = '从本地缓存中异步获取指定 key 对应的内容'
  s.expansion = "//从本地缓存中异步获取指定 key 对应的内容
wx.getStorage({
  key: 'key',
  success: function(res) {
      console.log(res.data)
  } 
})"
end
 

snippet 'wx.getStorageSync' do |s|
  s.trigger = 'wx.getStorageSync'
  s.description = '从本地缓存中同步获取指定 key 对应的内容'
  s.expansion = "//从本地缓存中同步获取指定 key 对应的内容
var value = wx.getStorageSync('key')
if (value) {
    // Do something with return value
}"
end


snippet 'wx.clearStorage' do |s|
  s.trigger = 'wx.clearStorage'
  s.description = '清理本地数据缓存'
  s.expansion = "//清理本地数据缓存
wx.clearStorage()"
end


snippet 'wx.clearStorageSync' do |s|
  s.trigger = 'wx.clearStorageSync'
  s.description = '同步清理本地数据缓存'
  s.expansion = "//同步清理本地数据缓存
try {
    wx.clearStorageSync()
} catch(e) {
}"
end


snippet 'wx.getLocation' do |s|
  s.trigger = 'wx.getLocation'
  s.description = '获取当前的地理位置、速度'
  s.expansion = "//获取当前的地理位置、速度
wx.getLocation({
  type: 'wgs84',
  success: function(res) {
    var latitude = res.latitude
    var longitude = res.longitude
    var speed = res.speed
    var accuracy = res.accuracy
  }
})"
end 
 
snippet 'wx.openLocation' do |s|
  s.trigger = 'wx.openLocation'
  s.description = '使用微信内置地图查看位置'
  s.expansion = "//使用微信内置地图查看位置
wx.getLocation({
  type: 'gcj02', //返回可以用于wx.openLocation的经纬度
  success: function(res) {
    var latitude = res.latitude
    var longitude = res.longitude
    wx.openLocation({
      latitude: latitude,
      longitude: longitude,
      scale: 28
    })
  }
})"
end 


snippet 'wx.getNetworkType' do |s|
  s.trigger = 'wx.getNetworkType'
  s.description = '获取网络类型'
  s.expansion = "//获取网络类型
wx.getNetworkType({
  success: function(res) {
    var networkType = res.networkType // 返回网络类型2g，3g，4g，wifi
  }
})"
end
  
snippet 'wx.getSystemInfo' do |s|
  s.trigger = 'wx.getSystemInfo'
  s.description = '获取系统信息'
  s.expansion = "//获取系统信息
wx.getSystemInfo({
  success: function(res) {
    console.log(res.model)
    console.log(res.pixelRatio)
    console.log(res.windowWidth)
    console.log(res.windowHeight)
    console.log(res.language)
    console.log(res.version)
  }
})"
end  
 
snippet 'wx.onAccelerometerChange' do |s|
  s.trigger = 'wx.onAccelerometerChange'
  s.description = '监听重力感应数据，频率：5次/秒'
  s.expansion = "//监听重力感应数据，频率：5次/秒
wx.onAccelerometerChange(function(res) {
  console.log(res.x)
  console.log(res.y)
  console.log(res.z)
})"
end
 
snippet 'wx.onCompassChange' do |s|
  s.trigger = 'wx.onCompassChange'
  s.description = '监听罗盘数据，频率：5次/秒'
  s.expansion = "//监听罗盘数据，频率：5次/秒
wx.onCompassChange(function (res) {
  console.log(res.direction)
})"
end 

snippet 'wx.setNavigationBarTitle' do |s|
  s.trigger = 'wx.setNavigationBarTitle'
  s.description = '动态设置当前页面的标题'
  s.expansion = "//动态设置当前页面的标题
wx.setNavigationBarTitle({
  title: '当前页面'
})"
end

snippet 'wx.navigateTo' do |s|
  s.trigger = 'wx.navigateTo'
  s.description = '保留当前页面，跳转到应用内的某个页面，使用wx.navigateBack可以返回到原页面'
  s.expansion = "//保留当前页面，跳转到应用内的某个页面，使用wx.navigateBack可以返回到原页面
wx.navigateTo({
  url: 'test?id=1'
})"
end

snippet 'wx.redirectTo' do |s|
  s.trigger = 'wx.redirectTo'
  s.description = '关闭当前页面，跳转到应用内的某个页面'
  s.expansion = "//关闭当前页面，跳转到应用内的某个页面
wx.redirectTo({
  url: 'test?id=1'
})"
end

snippet 'wx.navigateBack' do |s|
  s.trigger = 'wx.navigateBack'
  s.description = '关闭当前页面，回退前一页面'
  s.expansion = "//关闭当前页面，回退前一页面
wx.navigateBack()"
end


  
  
  


#=====微信小程序/end========================
end
