require 'ruble'
=begin 
 HBuilder可使用ruby脚本来扩展代码块和增强操作命令。这是极客的神奇玩具。
  本文档用于用户自定义JS扩展命令，并非HBuilder预置命令的文档，预置的代码块不可改。查阅预置代码块，请在弹出预置代码块界面时点右下角的编辑按钮，比如dw代码块。
  本文档修改完毕，保存即可生效。
  玩的愉快，别玩坏！
  
  脚本开源地址 https://github.com/dcloudio/HBuilderRubyBundle
  可以把你的配置共享到这里，也可以在这里获取其他网友的版本
  
  注：如果1.9版本之前的用户修改过代码块，请点右键打开本文档所在目录，找之前的snippets.rb.bak文件，把修改过的内容放置进来。 
=end

with_defaults :scope => "source.js" do #=====扩展定义JS代码块========================
#如下是一个示例代码块，可以复制后再添加新代码块
  snippet 'document.createElement()' do |s|            #document.createElement()是显示名称，代码助手提示列表显示时可见   
    s.trigger = "dc"                                   #dc是激活字符，即按下dc后会触发该代码块 
    s.expansion = "document.createElement(\"$1\")$0"   #expansion是代码块的输出内容，其中$0、$1是光标的停留和切换位置。$1是第一个停留光标，$0是最后回车时停留的光标。                        
                                                       #如果输出涉及到换行和tab，也需严格在这里使用换行和tab。                                                         
                                                       #输出双引号在前面加\来转义，输出$使用\$(单引号中)或\\$(双引号中)转义                                                                
    s.needApplyReContentAssist = true                  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出标签列表
  end
	
	#复制上述代码块，删除备注，在下面即可开始自定义
  snippet "console.log();" do |s|
    s.trigger = "clog"
    s.expansion = "console.log(\"$1\");"
  end
    
  snippet "return true;" do |s|
  s.trigger = "rtrue"
  s.expansion = "return true;"
  end
  
  snippet "return false;" do |s|
  s.trigger = "rfalse"
  s.expansion = "return false;"
  end
  
end


with_defaults :scope => "source.js", :input => :none, :output => :insert_as_snippet do |bundle|  #=====无显示名称的快捷命令=======================
=begin 
  command t(:multicomment) do |cmd| #:首先给命令命名,multicomment，该命令的目的是选中多行js后统一注释
    cmd.key_binding = 'M1+M2+/' #这里绑定触发按键，这里是Ctrl+Shift+/
    cmd.input = :selection #输入内容是选中区域的内容
    #以下是输出
    cmd.invoke do |context|
      selection = ENV['TM_SELECTED_TEXT'] || ''
      # 如果选区长度大于0，则输出如下字符。回车符就使用真实回车。如下输出即在选中内容前后加上/* */的注释
      if selection.length > 0
        "/*
${1:#{selection}}
*/"
      end
    end
  end
=end
end


# +----------------------------------------------------------------------
# | NEWTHINK JS HBuilder代码块
# +----------------------------------------------------------------------
# | NewThink[ WE CAN DO IT MORE SIMPLE ]
# +----------------------------------------------------------------------
# | Copyright (c) 2008-2016 http://www.newthink.cc All rights reserved.
# +----------------------------------------------------------------------
# | The MIT License (MIT)
# +----------------------------------------------------------------------
# | Author: hongweizhiyuan <403236160.com>
# +----------------------------------------------------------------------
# | Version: 1.0.0
# +----------------------------------------------------------------------

# 安装方法：
# 打开HBuilder 工具》扩展代码块》html代码
# 在最后加上nwthink_js_snippets.rb文件里的代码，如果已经加入，请覆盖升级

# NewThink JS标签代码块
# 使用方法
# 直接在js模板里敲  nt ，就会自动提示, nt 代表代码块前缀（NewThink 的缩写）以防和其它程序标签发生冲突
# 如A AddFavorite 标签 ，直接输入 ntAddFavorite试试

with_defaults :scope => "source.js" do #=====扩展定义JS代码块========================
    
    #加入收藏
    snippet 'nt AddFavorite' do |s|
      s.trigger = 'ntaddfavorite'
      s.description = '加入收藏'
      s.expansion = "//加入收藏
function AddFavorite(sURL, sTitle) {
    try {
        window.external.addFavorite(sURL, sTitle);
    } catch (e) {
        try {
            window.sidebar.addPanel(sTitle, sURL, \"\");
        } catch (e) {
            alert(\"加入收藏失败，请使用Ctrl+D进行添加\");
        }
    }
}"
    end
    
    
    
    #设为首页
    snippet 'nt SetHome' do |s|
      s.trigger = 'ntsethome'
      s.description = '设为首页'
      s.expansion = "//设为首页
function SetHome(obj, vrl) {
    try {
        obj.style.behavior = 'url(#default#homepage)';
        obj.setHomePage(vrl);
    } catch (e) {
        if (window.netscape) {
            try {
                netscape.security.PrivilegeManager.enablePrivilege(\"UniversalXPConnect\");
            } catch (e) {
                alert(\"此操作被浏览器拒绝！\\n请在浏览器地址栏输入\“about:config\”并回车\\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。\");
            }
            var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
            prefs.setCharPref('browser.startup.homepage', vrl);
        }
    }
}"
    end
    
    
    
        
  
end

# NewThink JS 模板标签代码块结束