require 'ruble'
=begin 
  本文档是php代码块的编辑文件。
 HBuilder可使用ruby脚本来编辑代码块和增强操作命令。
  编辑代码块：
  如果要新增一个代码块，复制如下一段代码到空白行，然后设定参数。
  'Style'是代码块的显示名字；
  s.trigger = 'style' 是设定激活字符，比如输入style均会在代码提示时显示该代码块；
  s.expansion = '' 是设定该代码块的输出字符，其中$0、$1是光标的停留和切换位置。
  snippet 'Style' do |s|
    s.trigger = 'style'
    s.expansion = '<style type="text/css" media="screen">
    $0
  </style>'
  end
  以上以HTML代码块做示例，其他代码块类似，使用时注意避免混淆
  
  操作时注意冲突，注意备份，有问题就还原。
  多看看已经写的ruby命令，会发现更多强大技巧。
  修改完毕，无需重启，自动生效。
  玩的愉快，别玩坏！
  
  注：如果1.9版本之前的用户修改过PHP代码块，请打开本文档目录，找之前的snippets.rb.bak文件，把修改过的内容放置进来，
  只需合并一次即可，如有疑问请联系管理员（QQ群：363040810）。
=end

#======begin======扩展定义PHP代码块============
with_defaults :scope => 'source.php' do

  snippet 'nt function ...' do |s|
    s.trigger = 'ntfun'
  s.expansion = '${1:public }function ${2:functionName}(\$${3:value}${4:=\'\'})
{
    ${0:// code...}
}'
  end
  
  
  snippet 'nt while ...' do |s|
    s.trigger = 'ntwhile'
    s.expansion = 'while (${1:$a <= 10}) {
    ${0:// code...}
}'
  end
  
  snippet 'ntp namespace admin' do |s|
    s.trigger = 'ntpnamespace'
    s.expansion = "
// +----------------------------------------------------------------------
// | NewThink [ Think In New World ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2066 http://www.newthink.cc All rights reserved.
// +----------------------------------------------------------------------
// | Author: Yang Hongwei <403236160@qq.com>
// +----------------------------------------------------------------------
// | Application: Ad
// +----------------------------------------------------------------------
namespace Admin\\Controller;
use Common\\Controller\\AdminbaseController;
class AdController extends AdminbaseController{
    
    
    
    
}"
  end
  
   snippet 'ntp namespace home' do |s|
    s.trigger = 'ntpnamespacehome'
    s.expansion = "
// +----------------------------------------------------------------------
// | NewThink [ Think In New World ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2066 http://www.newthink.cc All rights reserved.
// +----------------------------------------------------------------------
// | Author: Yang Hongwei <403236160@qq.com>
// +----------------------------------------------------------------------
// | Application: Ad
// +----------------------------------------------------------------------
namespace Portal\\Controller;
use Common\\Controller\\HomebaseController;
class AdController extends HomebaseController{
    
    
    
    
}"
  end 
    
    
#注释
snippet 'ntp notes' do |s|
  s.trigger = 'ntpnotes'
  s.description = '注释 控制器'
  s.expansion = "/**
* $1
*/"
end
    
#页码
snippet 'ntp page' do |s|
  s.trigger = 'ntppage'
  s.description = '页码'
  s.expansion = "\\$page = \\$this->page(\\$count, 20);"
end



 
##初始化
snippet 'ntp init' do |s|
  s.trigger = 'ntpinit'
  s.description = '初始化 控制器'
  s.expansion = "/**
     *  初始化
     */
    protected \\$ad_model;

    function _initialize() {
        parent::_initialize();
        \\$this->ad_model = D(\"Common/Ad\");
    }"
end


## index
snippet 'ntp index' do |s|
  s.trigger = 'ntpindex'
  s.description = '首页 控制器'
  s.expansion = "/**
 *  首页
 */
function index(){
    \\$ads=\\$this->ad_model->select();
    \\$this->assign(\"ads\",\\$ads);
    \\$this->display();
}"
end

##添加
snippet 'ntp add' do |s|
  s.trigger = 'ntpadd'
  s.description = '方法-添加'
  s.expansion = "/**
     *  添加
     */
    function add(){
    \\$this->display();
}"
end
  
##添加发送
snippet 'ntp add_post' do |s|
  s.trigger = 'ntpaddpost'
  s.description = '填加发送 控制器'
  s.expansion = "/**
     *  填加发送
     */
    function add_post(){
    if(IS_POST){
        if (\\$this->ad_model->create()){
            if (\\$this->ad_model->add()!==false) {
                \\$this->success(L('ADD_SUCCESS'), U(\"ad/index\"));
            } else {
                \\$this->error(L('ADD_FAILED'));
            }
        } else {
            \\$this->error($this->ad_model->getError());
        }

    }
}"
end


## 修改
snippet 'ntp edit' do |s|
  s.trigger = 'ntpedit'
  s.description = '修改 控制器'
  s.expansion = "/**
     *  修改
     */
    function edit(){
    \\$id=I(\"get.id\");
    \\$ad=$this->ad_model->where(\"ad_id=\\$id\")->find();
    \\$this->assign(\\$ad);
    \\$this->display();
}"
end


## 修改发送
snippet 'ntp edit_post' do |s|
  s.trigger = 'ntpeditpost'
  s.description = '修改发送 控制器'
  s.expansion = "/**
 *  修改发送
 */
function edit_post(){
    if (IS_POST) {
        if (\\$this->ad_model->create()) {
            if (\\$this->ad_model->save()!==false) {
                \\$this->success(\"保存成功！\", U(\"ad/index\"));
            } else {
                \\$this->error(\"保存失败！\");
            }
        } else {
            \\$this->error(\\$this->ad_model->getError());
        }
    }
}"
end

## 删除
snippet 'ntp del' do |s|
  s.trigger = 'ntpdel'
  s.description = '删除 控制器'
  s.expansion = "/**
 *  删除
 */
function delete(){
    \\$id = I(\"get.id\",0,\"intval\");
    if (\\$this->ad_model->delete(\\$id)!==false) {
        \\$this->success(\"删除成功！\");
    } else {
        \\$this->error(\"删除失败！\");
    }
}"
end


## toggle
snippet 'ntp toggle' do |s|
  s.trigger = 'ntptoggle'
  s.description = '显示隐藏 控制器'
  s.expansion = "/**
 *  切换
 */
function toggle(){
    if(isset(\\$_POST['ids']) && \\$_GET[\"display\"]){
        \\$ids = implode(\",\", \\$_POST['ids']);
        \\$data['status']=1;
        if (\\$this->ad_model->where(\"ad_id in (\\$ids)\")->save(\\$data)!==false) {
            \\$this->success(\"显示成功！\");
        } else {
            \\$this->error(\"显示失败！\");
        }
    }
    if(isset(\\$_POST['ids']) && \\$_GET[\"hide\"]){
        \\$ids = implode(\",\", \\$_POST['ids']);
        \\$data['status']=0;
        if (\\$this->ad_model->where(\"ad_id in (\\$ids)\")->save(\\$data)!==false) {
            \\$this->success(\"隐藏成功！\");
        } else {
            \\$this->error(\"隐藏失败！\");
        }
    }
}"
end

  
##案例
snippet 'ntp demo' do |s|
  s.trigger = 'ntpdemo'
  s.description = '广告案例'
  s.expansion = "
namespace Admin\\Controller;
use Common\\Controller\\AdminbaseController;
class AdController extends AdminbaseController{
    
    
    protected \\$ad_model;

    /**
     *  初始化
     */
    function _initialize() {
        parent::_initialize();
        \\$this->ad_model = D(\"Common/Ad\");
    }

    /**
     *  首页
     */
    function index(){
        \\$ads=\\$this->ad_model->select();
        \\$this->assign(\"ads\",\\$ads);
        \\$this->display();
    }

    /**
     *  填加
     */
    function add(){
        \\$this->display();
    }

    /**
     *  填加发送
     */
    function add_post(){
        if(IS_POST){
            if (\\$this->ad_model->create()){
                if (\\$this->ad_model->add()!==false) {
                    \\$this->success(L('ADD_SUCCESS'), U(\"ad/index\"));
                } else {
                    \\$this->error(L('ADD_FAILED'));
                }
            } else {
                \\$this->error(\\$this->ad_model->getError());
            }

        }
    }

    /**
     *  修改
     */
    function edit(){
        \\$id=I(\"get.id\");
        \\$ad=\\$this->ad_model->where(\"ad_id=\\$id\")->find();
        \\$this->assign(\\$ad);
        \\$this->display();
    }

    /**
     *  修改发送
     */
    function edit_post(){
        if (IS_POST) {
            if (\\$this->ad_model->create()) {
                if (\\$this->ad_model->save()!==false) {
                    \\$this->success(\"保存成功！\", U(\"ad/index\"));
                } else {
                    \\$this->error(\"保存失败！\");
                }
            } else {
                \\$this->error(\\$this->ad_model->getError());
            }
        }
    }

    /**
     *  删除
     */
    function delete(){
        \\$id = I(\"get.id\",0,\"intval\");
        if (\\$this->ad_model->delete(\\$id)!==false) {
            \\$this->success(\"删除成功！\");
        } else {
            \\$this->error(\"删除失败！\");
        }
    }

    /**
     *  切换
     */
    function toggle(){
        if(isset(\\$_POST['ids']) && \\$_GET[\"display\"]){
            \\$ids = implode(\",\", \\$_POST['ids']);
            \\$data['status']=1;
            if (\\$this->ad_model->where(\"ad_id in (\\$ids)\")->save(\\$data)!==false) {
                \\$this->success(\"显示成功！\");
            } else {
                \\$this->error(\"显示失败！\");
            }
        }
        if(isset(\\$_POST['ids']) && \\$_GET[\"hide\"]){
            \\$ids = implode(\",\", \\$_POST['ids']);
            \\$data['status']=0;
            if (\\$this->ad_model->where(\"ad_id in (\\$ids)\")->save(\\$data)!==false) {
                \\$this->success(\"隐藏成功！\");
            } else {
                \\$this->error(\"隐藏失败！\");
            }
        }
    }

}"
end  
  
  
  
  
  
snippet 'ntp select' do |s|
  s.trigger = 'ntpselect'
  s.description = 'select语句'
  s.expansion = "\\$$1 = D('$2')->select();"
end
  
  
  
#######################################################################################################################
snippet 'MODULE_NAME' do |s|
  s.trigger = 'modulename'
  s.description = '模块名儿 MODULE_NAME'
  s.expansion = "MODULE_NAME"
end


snippet 'foreach($a as $key =>$val)' do |s|
  s.trigger = 'foreachkey'
  s.description = '含有key的foreach'
  s.expansion = "foreach (\\$$1 as \\$key => \\$val) {
       $2     
}"
end

snippet 'foreach($a as $val)' do |s|
  s.trigger = 'foreach '
  s.description = '不含有key的foreach'
  s.expansion = "foreach (\\$$1 as \\$val) {
  $0        
}"
end

snippet 'print_r' do |s|
  s.trigger = 'printr'
  s.description = 'print_r'
  s.expansion = "print_r(\\$$1);"
end



snippet 'echo $a' do |s|
  s.trigger = 'echo'
  s.description = 'echo'
  s.expansion = "echo \\$$1;"
end


####### header/start ################################################################################################################ 

snippet 'header(\'Location:\'.U(\'index/index\'));' do |s|
  s.trigger = 'headerU'
  s.description = 'header'
  s.expansion = "header('Location:'.U('$1/$0'));"
end


snippet 'header("HTTP/1.0 404 Not Found");' do |s|
  s.trigger = 'header404'
  s.description = 'header("HTTP/1.0 404 Not Found");'
  s.expansion = "header(\"HTTP/1.0 404 Not Found\");"
end

####### header/end ################################################################################################################ 
  
####### display/start ################################################################################################################ 


snippet '$this->display("/public:404"); ' do |s|
  s.trigger = 'display404'
  s.description = '\\$this->display(\"/public:404\"); '
  s.expansion = "\\$this->display(\"/public:404\"); "
end


snippet '$this->display("index"); ' do |s|
  s.trigger = 'displaypage'
  s.description = '\\$this->display(\"index\"); '
  s.expansion = "\\$this->display(\"$1\"); "
end


####### display/end ################################################################################################################ 
  



####### header/start ################################################################################################################ 
  
snippet 'header utf8' do |s|
  s.trigger = 'headerutf8'
  s.description = 'header(\'Content-Type: text/html; charset=UTF-8\');'
  s.expansion = "header('Content-Type: text/html; charset=UTF-8');"
end


snippet 'header gb2312' do |s|
  s.trigger = 'headergb2312'
  s.description = 'header(\'Content-Type: text/html; charset=GB2312\');'
  s.expansion = "header('Content-Type: text/html; charset=GB2312');"
end

####### header/end ################################################################################################################ 
  


####### Huayu / Soap / start ################################################################################################################ 


# 华宇 命名空间
snippet 'HY namespace' do |s|
  s.trigger = 'hynamespace'
  s.description = '初始化namespace'
  s.expansion = "// +----------------------------------------------------------------------
// | NewThink [ Think In New World ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016-2017 http://www.sxctkj.cc All rights reserved.
// +----------------------------------------------------------------------
// | Author: 山西创泰科技
// +----------------------------------------------------------------------
// | Application: index
// +----------------------------------------------------------------------
namespace Home\\Controller;
use Think\\Controller;
class IndexController extends Controller{

    //index
    public function index(){
        $1
    }
    
    //demo
    public function demo(){
        $0
    }
    
}"
end


# 华宇 命名空间
snippet 'HY public function' do |s|
  s.trigger = 'hypublicfunction'
  s.description = 'public function'
  s.expansion = "//demo
public function $1(){
    $0
}"
end


#xml转array
snippet 'HY fun sp_xml2array' do |s|
  s.trigger = 'hyfunxml2array'
  s.description = 'xml转array'
  s.expansion = "//解析XML为数组  
function sp_Xml2Array(\\$xml){  
    if(!\\$xml) return array();  
    \\$simplexml_obj = simplexml_load_string(\\$xml);  
    \\$json = json_encode(\\$simplexml_obj);  
    return json_decode(\\$json,TRUE);  
} "
end

 
# 华宇 soap 会员远程地址
snippet 'HY Soap Member url' do |s|
  s.trigger = 'hysoapmemberurl'
  s.description = '华宇 soap 会员远程地址'
  s.expansion = "//会员信息
\\$soap = new \\SoapClient(null,array('location'=>'http://202.97.143.124:1084/jddjk_wx.php','uri'=>'http://202.97.143.124:1084'));"
end

# 华宇 soap 会员远程地址
snippet 'HY Soap SendSMS url' do |s|
  s.trigger = 'hysoapsendsmsurl'
  s.description = '华宇 soap 短信远程地址'
  s.expansion = "//发送短信
\\$soap = new \\SoapClient(null,array('location'=>'http://202.97.143.124:1084/sms.php','uri'=>'http://202.97.143.124:1084'));"
end


## 根据联系电话，获取会员基本信息
snippet 'HY GetHYXXByLXDH' do |s|
  s.trigger = 'hygethyxxbylxdh'
  s.description = '根据联系电话，获取会员基本信息'
  s.expansion = "\\$soap->GetHYXXByLXDH('13934529163'); "
end


## 根据人员ID，获取卡片列表
snippet 'HY GetHYKXX' do |s|
  s.trigger = 'hygethykxx'
  s.description = '根据人员ID，获取卡片列表'
  s.expansion = "\\$soap->GetHYKXX(1);"
end


## 根据会员卡片ID，获取一维码、二维码字符串
snippet 'HY GetHYKCode' do |s|
  s.trigger = 'hygethykcode'
  s.description = '根据会员卡片ID，获取一维码、二维码字符串'
  s.expansion = "\\$soap->GetHYKCode(400000006);"
end

## 获取会员指定卡片的间夜券列表
snippet 'HY GetJYQ_List' do |s|
  s.trigger = 'hygetjyqlist'
  s.description = '获取会员指定卡片的间夜券列表'
  s.expansion = "\\$soap->GetJYQ_List(400000006,'wks');"
end


## 获取会员指定卡片的间夜券使用记录
snippet 'HY GetJYQSYJL' do |s|
  s.trigger = 'hygetjyqsyjl'
  s.description = '获取会员指定卡片的间夜券使用记录'
  s.expansion = "\\$soap->GetJYQSYJL(400000006);"
end

## 获取指定间夜券使用记录的起止日期
snippet 'HY GetJYQSYJL_JYQXX' do |s|
  s.trigger = 'hygetjyqsyjljyqxx'
  s.description = '获取指定间夜券使用记录的起止日期'
  s.expansion = "\\$soap->GetJYQSYJL_JYQXX(400000027);"
end


## 发送短信
snippet 'HY SendSMS' do |s|
  s.trigger = 'hysendsms'
  s.description = '短信SOAP 及 发送短信函数'
  s.expansion = "//发送短信
\\$soapsms = new \\SoapClient(null,array('location'=>'http://202.97.143.124:1084/sms.php','uri'=>'http://202.97.143.124:1084'));
\\$soapsms->SendSMS('13934529163', '【华宇集团】动态验证码888888（60秒内有效）。请注意保密，勿将验证码告知他人。')"
end



## demo
snippet 'HY Demo' do |s|
  s.trigger = 'hydemo'
  s.description = '华宇demo'
  s.expansion = "header('Content-Type: text/html; charset=UTF-8');
    
//会员信息
\\$soap = new \\SoapClient(null,array('location'=>'http://202.97.143.124:1084/jddjk_wx.php','uri'=>'http://202.97.143.124:1084'));

echo '//根据联系电话，获取会员基本信息<br/>';
\\$a = \\$soap->GetHYXXByLXDH('13934529163');
dump(sp_Xml2Array(\\$a));
echo \"<br/><br/>\";

echo '//根据人员ID，获取卡片列表<br/>';
\\$b = \\$soap->GetHYKXX(1);
dump(sp_Xml2Array(\\$b));
echo \"<br/><br/>\";

echo '//根据会员卡片ID，获取一维码、二维码字符串<br/>';
\\$c = \\$soap->GetHYKCode(400000006); 
dump(sp_Xml2Array(\\$c));
echo \"<br/><br/>\";

echo '//获取会员指定卡片的间夜券列表<br/>';
\\$d = \\$soap->GetJYQ_List(400000006,'wks'); 
dump(sp_Xml2Array(\\$d));
echo \"<br/><br/>\";

echo '//获取会员指定卡片的间夜券使用记录<br/>';
\\$e = \\$soap->GetJYQSYJL(400000006); 
dump(sp_Xml2Array(\\$e));
echo \"<br/><br/>\";

echo '//获取指定间夜券使用记录的起止日期<br/>';
\\$f = \\$soap->GetJYQSYJL_JYQXX(400000027);
dump(sp_Xml2Array(\\$f));
echo \"<br/><br/>\";


echo \"<br/>\";  "
end
####### Huayu / Soap / end ################################################################################################################ 





  
  
end 
#======end======扩展定义PHP代码块结束============

#======begin======扩展定义PHP中html代码块============
with_defaults :scope => 'meta.project.com.aptana.editor.php.phpNature meta.project.com.aptana.projects.webnature text.html.basic - source' do
=begin   
  示例：
  snippet '<?php ... ?>' do |s|
    s.trigger = 'php'
    s.expansion = '<?${TM_PHP_OPEN_TAG:php} $0 ?>'
  end
=end
 #从这里开始自定义
 
end
