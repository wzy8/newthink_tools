require 'ruble'
=begin 
 HBuilder可使用ruby脚本来扩展代码块和增强操作命令。这是极客的神奇玩具。
  本文档用于用户自定义HTML扩展命令，并非HBuilder预置命令的文档，预置的代码块不可改。查阅预置代码块，请在弹出预置代码块界面时点右下角的编辑按钮，比如div代码块。
  本文档修改完毕，保存即可生效。
  玩的愉快，别玩坏！
  
  脚本开源地址 https://github.com/dcloudio/HBuilderRubyBundle
  可以把你的配置共享到这里，也可以在这里获取其他网友的版本
  
  注：如果1.9版本之前的用户修改过HTML代码块，请点右键打开本文档所在目录，找之前的snippets.rb.bak文件，把修改过的内容放置进来。 
=end

with_defaults :scope => 'text.html text' do |bundle|  #=====HTML标签代码块================================================================================
#如下是一个示例代码块，可以复制后再添加新代码块
  snippet 'div_class' do |cmd|  #div_class是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'divc'        #divc是激活字符，即按下divc后会触发该代码块
    cmd.expansion = "<div class=\"$1\">
    $0
</div>"                         #expansion是代码块的输出内容，其中$0、$1是光标的停留和切换位置。$1是第一个停留光标，$0是最后回车时停留的光标。
                                                          #如果输出涉及到换行和tab，也需严格在这里使用换行和tab。
                                                          #输出双引号在前面加\来转义，输出$使用\$(单引号中)或\\$(双引号中)转义
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #div_class代码块结束
  
  snippet 'ng-pluralize' do |cmd|
    cmd.trigger = 'ngp'
    cmd.expansion = "<ng-pluralize>$1</ng-pluralize>"
  end

end


with_defaults :scope => 'text.html entity.other.attribute-name.html' do |bundle|  #=====HTML属性代码块====================================================
#如下是一个示例代码块，可以复制后再添加新代码块
  snippet 'ng-' do |s|   #ng-是显示名称，代码助手提示列表显示时可见
    s.trigger = 'ng-'        #ng-是激活字符，即按下ng-后会触发该代码块
    s.expansion='ng-${1:app/bind/bind-html/bind-template/blur/change/checked/class/class-even/class-odd/click/cloak/controller/copy/csp/cut/dblclick/disabled/focus/hide/href/if/include/init/keydown/keypress/keyup/list/model/mousedown/mouseenter/mouseleave/mousemove/mouseover/mouseup/ng-non-bindable/open/options/paste/readonly/repeat-start/repeat-end/selected/show/src/srcset/style/submit/swipe-left/swipe-right/switch/switch-default/switch-when/view}="$2"'
        #expansion是代码块的输出内容，其中$0、$1是光标的停留和切换位置。
      #$1是第一个停留光标，$0是最后回车时停留的光标。
      #使用{}包围的内容，是提示值域。
      #如果输出涉及到换行和tab，也需严格在这里使用换行和tab。
      #输出双引号在前面加\来转义，输出$使用\$(单引号中)或\\$(双引号中)转义
    s.locationType='HTML_ATTRIBUTE'
  end #ng代码块结束

end


with_defaults :scope => 'text.html - source', :input => :none, :output => :insert_as_snippet do |bundle|  #=====无显示名称的快捷命令=======================
=begin
如下示例均为系统已经预置的命令，无需重复制作
示例1 Ctrl+Enter输出<br />
  command t(:quick_br) do |cmd|
    cmd.key_binding = 'M2+ENTER'
    cmd.output = :insert_as_snippet
    cmd.input = :none
    cmd.invoke { "<br />" }
  end
示例2 Ctrl+9为选中文字添加包围标签
  command t(:wrap_selection_in_tag_pair) do |cmd|
    cmd.key_binding = "CONTROL+9"
    cmd.input = :selection
    cmd.invoke do |context|
      selection = ENV['TM_SELECTED_TEXT'] || ''
      if selection.length > 0
        "<${1:p}>#{selection.gsub('/', '\/')}</${1:p}>"
      else
        "<${1:p}>$0</${1:p}>"
      end
    end
  end
=end
#可复制一段命令，在下面开始制作新命令
  
end

# +----------------------------------------------------------------------
# | ThinkPHP NewThink HBuilder代码块
# +----------------------------------------------------------------------
# | NewThink[ WE CAN DO IT MORE SIMPLE ]
# +----------------------------------------------------------------------
# | Copyright (c) 2008-2016 http://www.newthink.cc All rights reserved.
# +----------------------------------------------------------------------
# | The MIT License (MIT)
# +----------------------------------------------------------------------
# | Author: hongweizhiyuan <403236160.com>
# +----------------------------------------------------------------------
# | Version: 1.0.0
# +----------------------------------------------------------------------

# 安装方法：
# 打开HBuilder 工具》扩展代码块》html代码
# 在最后加上html_snippets.rb文件里的代码，如果已经加入，请覆盖升级

#ThinkPHP NewThink 模板标签代码块
# 使用方法
# 直接在html模板里敲tc，就会自动提示,tc代表代码块前缀（thinkphp thinkcmf缩写）以防和其它程序标签发生冲突
# 我们是用  thinkcmf 底层框架进行开发的
# 如foreach标签 ，直接输入tcforeach试试


with_defaults :scope => 'text.html text' do |bundle|  #=====HTML标签代码块================================================================================

  snippet 'tcforeach' do |cmd|  #tcforeach是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcforeach'        #tcforeach是激活字符，即按下tcforeach后会触发该代码块
    cmd.expansion = "<foreach name=\"$1\" item=\"vo\">
    $0
</foreach>"                         #expansion是代码块的输出内容，其中$0、$1是光标的停留和切换位置。$1是第一个停留光标，$0是最后回车时停留的光标。
                                                          #如果输出涉及到换行和tab，也需严格在这里使用换行和tab。
                                                          #输出双引号在前面加\来转义，输出$使用$$转义
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #foreach代码块结束
  
  snippet 'tcvolist' do |cmd|  #tcvolist是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcvolist'        #tcvolist是激活字符，即按下tcvolist后会触发该代码块
    cmd.expansion = "<volist name=\"$1\" id=\"vo\">
    $0
</volist>" 
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #volist代码块结束
  
  snippet 'tcfor' do |cmd|  #tcfor是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcfor'        #tcfor是激活字符，即按下tcfor后会触发该代码块
    cmd.expansion = "<for start=\"$1\" end=\"$2\" comparison=\"lt$3\" step=\"1$4\" name=\"i$5\" >
    $0
</for>" 
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #for代码块结束
  
  snippet 'tcswitch' do |cmd|  #tcswitch是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcswitch'        #tcswitch是激活字符，即按下tcswitch后会触发该代码块
    cmd.expansion = "<switch name=\"$1\" >
    <case value=\"$2\" break=\"$3\">$4</case>
    <case value=\"$5\">$6</case>
    <default />$0
</switch>" 
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #switch代码块结束
  
  snippet 'tceq' do |cmd|  #tceq是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tceq'        #tceq是激活字符，即按下tceq后会触发该代码块
    cmd.expansion = '<${1:eq/equal/neq/notequal/gt/egt/lt/elt/heq/nheq} name="$2" value="$3">
    $0
</$1>'
  end #比较代码块结束
  
  snippet 'tcin' do |cmd|  #tcin是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcin'        #tcin是激活字符，即按下tcin后会触发该代码块
    cmd.expansion = '<${1:in/notin/between/notbetween} name="$2" value="$3">
    $0
</$1>'
  end #范围判断代码块结束
  
  snippet 'tcif' do |cmd|  #tcif是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcif'        #tcif是激活字符，即按下tcif后会触发该代码块
    cmd.expansion = '<if condition="$1">
    $0    
</if>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #if代码块结束
  
  snippet 'tcifelse' do |cmd|  #tcifelse是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcifelse'        #tcifelse是激活字符，即按下tcifelse后会触发该代码块
    cmd.expansion = '<if condition="$1">
    $0$2
    <else />
    $3
</if>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #ifelse代码块结束
  
  snippet 'tcifelseif' do |cmd|  #tcifelseif是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcifelseif'        #tcifelseif是激活字符，即按下tcifelseif后会触发该代码块
    cmd.expansion = '<if condition="$1"> 
        $0$2
    <elseif condition="$3"/>
        $4
    <else /> 
        $5
</if>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #ifelseif代码块结束
  
  snippet 'tcpresent' do |cmd|  #tcpresent是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcpresent'        #tcpresent是激活字符，即按下tcpresent后会触发该代码块
    cmd.expansion = '<present name="$1">
    $0
</present>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #present代码块结束
  
  snippet 'tcnotpresent' do |cmd|  #tcnotpresent是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcnotpresent'        #tcnotpresent是激活字符，即按下tcnotpresent后会触发该代码块
    cmd.expansion = '<notpresent name="$1">
    $0
</notpresent>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #notpresent代码块结束
  
  snippet 'tcempty' do |cmd|  #tcempty是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcempty'        #tcempty是激活字符，即按下tcempty后会触发该代码块
    cmd.expansion = '<empty name="$1">
    $0
</empty>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #empty代码块结束
  
  snippet 'tcdefined' do |cmd|  #tcdefined是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcdefined'        #tcdefined是激活字符，即按下tcdefined后会触发该代码块
    cmd.expansion = '<defined name="$1">
    $0
</defined>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #defined代码块结束
  
  snippet 'tcphp' do |cmd|  #tcphp是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcphp'        #tcphp是激活字符，即按下tcphp后会触发该代码块
    cmd.expansion = '<php>
    $0
</php>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #php代码块结束
  
  snippet 'tcimport' do |cmd|  #tcimport是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcimport'        #tcimport是激活字符，即按下tcimport后会触发该代码块
    cmd.expansion = '<import file="$1" type="js$2">
    $0
</import>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #import代码块结束
  
  snippet 'tcload' do |cmd|  #tcload是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcload'        #tcload是激活字符，即按下tcload后会触发该代码块
    cmd.expansion = '<load href="$1">
    $0
</load>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #load代码块结束
  
  snippet 'tcjs' do |cmd|  #tcjs是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcjs'        #tcjs是激活字符，即按下tcjs后会触发该代码块
    cmd.expansion = '<js href="$1">
    $0
</js>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #js代码块结束
  
  snippet 'tccss' do |cmd|  #tccss是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tccss'        #tccss是激活字符，即按下tccss后会触发该代码块
    cmd.expansion = '<css href="$1">
    $0
</css>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #css代码块结束
  
  snippet 'tctcinclude' do |cmd|  #tctcinclude是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tctcinclude'        #tctcinclude是激活字符，即按下tctcinclude后会触发该代码块
    cmd.expansion = '<tc_include file="Public:$0"/>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #tcinclude代码块结束
  
  snippet 'tcinclude' do |cmd|  #tcinclude是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcinclude'        #tcinclude是激活字符，即按下tcinclude后会触发该代码块
    cmd.expansion = '<include file="$0" >
        $0
</include>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #include代码块结束
  
  snippet 'tcliteral' do |cmd|  #tcliteral是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcliteral'        #tcliteral是激活字符，即按下tcliteral后会触发该代码块
    cmd.expansion = '<literal>
    $0
</literal>'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #literal代码块结束
  
  snippet 'tcroot' do |cmd|  #tcroot是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcroot'        #tcroot是激活字符，即按下tcroot后会触发该代码块
    cmd.expansion = '__ROOT__'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #root代码块结束
  
  snippet 'tctmpl' do |cmd|  #tctmpl是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tctmpl'        #tctmpl是激活字符，即按下tctmpl后会触发该代码块
    cmd.expansion = '__TMPL__'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #tmpl代码块结束
  
  snippet 'tcstatics' do |cmd|  #tcstatics是显示名称，代码助手提示列表显示时可见
    cmd.trigger = 'tcstatics'        #tcstatics是激活字符，即按下tcstatics后会触发该代码块
    cmd.expansion = '__STATICS__'
    cmd.needApplyReContentAssist = true  #这句话的意思是输出后同时激活代码助手，即在$1的位置直接拉出样式列表
  end #statics代码块结束
  
  # ===================site 系统设置开始===================
  
  #站点名称
  snippet 'nt site_name' do |cmd|
    cmd.description = '站点名称'
    cmd.trigger = 'ntsite_name'
    cmd.expansion = '{\$site_name}'
    cmd.needApplyReContentAssist = true 
  end
  
  #站点域名
  snippet 'nt site_host' do |cmd|
    cmd.description = '站点域名'
    cmd.trigger = 'ntsite_host'
    cmd.expansion = '{\$site_host}'
    cmd.needApplyReContentAssist = true 
  end
  
  #安装目录
  snippet 'nt site_root' do |cmd|
    cmd.description = '安装目录'
    cmd.trigger = 'ntsite_root'
    cmd.expansion = '{\$site_root}'
    cmd.needApplyReContentAssist = true 
  end
  
  #icp备案信息
  snippet 'nt site_icp' do |cmd|
    cmd.description = 'ICP备案信息'
    cmd.trigger = 'ntsite_icp'
    cmd.expansion = '<a href="http://www.miitbeian.gov.cn/publish/query/indexFirst.action" target="_blank">{\$site_icp}</a>'
    cmd.needApplyReContentAssist = true 
  end
  
  #公安局备案信息
  snippet 'nt site_police' do |cmd|
    cmd.description = '公安局备案信息'
    cmd.trigger = 'ntsite_police'
    cmd.expansion = '{\$site_police}'
    cmd.needApplyReContentAssist = true 
  end
  
  #公司地址
  snippet 'nt site_company_address' do |cmd|
    cmd.description = '公司地址'
    cmd.trigger = 'ntsite_company_address'
    cmd.expansion = '{\$site_company_address}'
    cmd.needApplyReContentAssist = true 
  end
  
  #公司电话
  snippet 'nt site_company_telphone' do |cmd|
    cmd.description = '公司电话'
    cmd.trigger = 'ntsite_company_tel'
    cmd.expansion = '{\$site_company_tel}'
    cmd.needApplyReContentAssist = true 
  end
  
  #管理员邮箱
  snippet 'nt site_admin_email' do |cmd|
    cmd.description = '管理员邮件'
    cmd.description = '管理员邮件'
    cmd.trigger = 'ntsite_admin_email'
    cmd.expansion = '{\$site_admin_email}'
    cmd.needApplyReContentAssist = true 
    
  end
  
  #版权信息
  snippet 'nt site_copyright' do |cmd|
    cmd.description = '版权信息'
    cmd.trigger = 'ntsite_copyright'
    cmd.expansion = '{\$site_copyright}'
    cmd.needApplyReContentAssist = true 
  end
  
  #页面统计代码
  snippet 'nt site_tongji' do |cmd|
    cmd.description = '页面统计代码'
    cmd.trigger = 'ntsite_tongji'
    cmd.expansion = '{\$site_tongji}'
    cmd.needApplyReContentAssist = true 
  end
  
  #SEO标题
  snippet 'nt site_seo_title' do |cmd|
    cmd.description = 'SEO标题'
    cmd.trigger = 'ntsite_seo_title'
    cmd.expansion = '{\$site_seo_title}'
    cmd.needApplyReContentAssist = true 
  end
  
  #SEO关键字
  snippet 'nt site_seo_keywords' do |cmd|
    cmd.description = 'SEO关键字'
    cmd.trigger = 'ntsite_seo_keywords'
    cmd.expansion = '{\$site_seo_keywords}'
    cmd.needApplyReContentAssist = true 
  end
  
  #SEO描述
  snippet 'nt site_seo_description' do |cmd|
    cmd.description = 'SEO描述'
    cmd.trigger = 'ntsite_seo_description'
    cmd.expansion = '{\$site_seo_description}'
    cmd.needApplyReContentAssist = true 
  end
  
  
# ===================site 系统设置结束===================


############################### main/start #########################################


  #模板下的 public 路径
  snippet 'nt public' do |cmd|
    cmd.description = '模板下的 public 路径'
    cmd.trigger = 'ntpublic'
    cmd.expansion = '__TMPL__Public'
    cmd.needApplyReContentAssist = true 
  end


  #无分页列表
  snippet 'nt arclist' do |cmd|
    cmd.description = '无分页列表'
    cmd.trigger = 'ntarclist'
    cmd.expansion = '<php>
    \$tag = \'cid:$1;order:listorder asc\';
    \$pagesize =\'$2\';
    \$content=sp_sql_posts_paged(\$tag,\$pagesize);
</php>

<foreach name="content[\'posts\']" item="vo">
    <php>\$smeta=json_decode(\$vo[\'smeta\'], true);</php>
    {:leuu(\'article/index\',array(\'id\'=>\$vo[\'tid\']))}<br/>
    {\$vo.post_title }<br>
    {\$vo.post_content |msubstr=0,$3}<br>
    {:date(\'Y-m-d\',strtotime(\$vo[\'post_date\']))}<br>
    <img src="{:sp_get_asset_upload_path(\$smeta[\'thumb\'])}"/>
</foreach>'
    cmd.needApplyReContentAssist = true 
  end


#有分页列表
  snippet 'nt list' do |cmd|
    cmd.description = '有分页列表'
    cmd.trigger = 'ntlist'
    cmd.expansion = '<php>\$lists = sp_sql_posts_paged("cid:\$cat_id;order:post_date DESC;",10);</php>
<volist name="lists[\'posts\']" id="vo">
<php>\$smeta=json_decode(\$vo[\'smeta\'], true);</php>

<div class="list-boxes">
    <a href="{:leuu(\'article/index\',array(\'id\'=>\$vo[\'tid\']))}">{\$vo.post_title}</a>
    {:date(\'Y-m-d\',strtotime(\$vo[\'post_date\']))}<br>
    <p>{\$vo.post_excerpt}</p>
    <div>
    <div class="pull-left">
        <div class="list-actions">
    <a href="javascript:;"><i class="fa fa-eye"></i><span>{\$vo.post_hits}</span></a>
    <a href="{:U(\'article/do_like\',array(\'id\'=>\$vo[\'object_id\']))}" class="js-count-btn"><i class="fa fa-thumbs-up"></i><span class="count">{\$vo.post_like}</span></a>
    <a href="{:U(\'user/favorite/do_favorite\',array(\'id\'=>\$vo[\'object_id\']))}" class="js-favorite-btn" data-title="{\$vo.post_title}" data-url="{:U(\'portal/article/index\',array(\'id\'=>\$vo[\'tid\']))}" data-key="{:sp_get_favorite_key(\'posts\',\$vo[\'object_id\'])}">i class="fa fa-star-o"></i></a>
    </div>
    </div>
    <a class="btn btn-warning pull-right" href="{:leuu(\'article/index\',array(\'id\'=>\$vo[\'tid\']))}">查看更多</a>
</div>
</div>
</volist>

<div class="pagination">
    <ul>
    {\$lists[\'page\']}
    </ul>
</div>'
    cmd.needApplyReContentAssist = true 
  end

#文章页
snippet 'nt article' do |cmd|
    cmd.description = '文章页'
    cmd.trigger = 'ntarticle'
    cmd.expansion = '
标题：{\$post_title}
日期：{\$post_date}
作者：{\$user_nicename|default=\$user_login}
浏览次数：<a href="javascript:;"><i class="fa fa-eye"></i><span>{\$post_hits}</span></a>
占赞：<a href="{:U(\'article/do_like\',array(\'id\'=>\$object_id))}" class="js-count-btn"><i class="fa fa-thumbs-up"></i><span class="count">{\$post_like}</span></a>
收藏：<a href="{:U(\'user/favorite/do_favorite\',array(\'id\'=>\$object_id))}" class="js-favorite-btn" data-title="{$post_title}" data-url="{:leuu(\'article/index\',array(\'id\'=>\$tid,\'cid\'=>\$term_id))}" data-key="{:sp_get_favorite_key(\'posts\',\$object_id)}"><i class="fa fa-star-o"></i></a>
内容：{\$post_content}'
    cmd.needApplyReContentAssist = true 
end


#单页面
snippet 'nt page' do |cmd|
    cmd.description = '单页面'
    cmd.trigger = 'ntpage'
    cmd.expansion = '{\$post_title}
{\$post_content}'
    cmd.needApplyReContentAssist = true 
end






#热门文章
snippet 'nt listhot' do |cmd|
    cmd.description = '热门文章'
    cmd.trigger = 'ntlisthot'
    cmd.expansion = '<php>
    \$hot_articles=sp_sql_posts("cid:\$portal_hot_articles;order:post_hits desc;limit:5;"); 
</php>
<foreach name="hot_articles" item="vo">
    <php>\$top=\$key<3?"top3":"";</php>
    <li class="{\$top}"><i>{\$key+1}</i><a title="{\$vo.post_title}" href="{:leuu(\'article/index\',array(\'id\'=>$vo[\'tid\'],\'cid\'=>\$vo[\'term_id\']))}">{\$vo.post_title}</a></li>
</foreach>'
cmd.needApplyReContentAssist = true 
end
  
  

  #友情链接
  snippet 'nt links' do |cmd|
    cmd.description = '友情链接'
    cmd.trigger = 'ntlinks'
    cmd.expansion = '<php>\$links=sp_getlinks();</php>
<foreach name="links" item="vo">
    {\$vo.link_name}<br/>
    {\$vo.link_url}<br/>
    {\$vo.link_target}<br/>
    {\$vo.link_description}<br/>

    <a href="{\$vo.link_url}" target="{\$vo.link_target}">{\$vo.link_name}</a>
    
    <option value=\'{\$vo.link_url}\'>{\$vo.link_name}</option>
</foreach>'
    cmd.needApplyReContentAssist = true 
  end
  
############################### main/end #########################################


############################### URL/start #########################################

  #列表URL_大U
  snippet 'nt url_list U' do |cmd|
    cmd.description = '列表URL_大U'
    cmd.trigger = 'nturllistu'
    cmd.expansion = '{:leuu(\'list/index\',array(\'id\'=>\$vo[\'termid\']))}'
    cmd.needApplyReContentAssist = true 
  end
  
  #列表URL_大U 指定id
  snippet 'nt url_list U id' do |cmd|
    cmd.description = '列表URL_大U'
    cmd.trigger = 'nturllistuid'
    cmd.expansion = '{:leuu(\'list/index\',array(\'id\'=>$1))}'
    cmd.needApplyReContentAssist = true 
  end
  
  
 #列表URL
  snippet 'nt url_list id' do |cmd|
    cmd.description = '列表URL'
    cmd.trigger = 'nturllistid'
    cmd.expansion = '{\$site_host}/index.php?g=portal&m=list&a=index&id=$0'
    cmd.needApplyReContentAssist = true 
  end

  #单页面 URL_大U 指定id
  snippet 'nt url_page U id' do |cmd|
    cmd.description = '单页面URL_大U'
    cmd.trigger = 'nturlpageuid'
    cmd.expansion = '{:leuu(\'page/index\',array(\'id\'=>$1))}'
    cmd.needApplyReContentAssist = true 
  end
  
  
  #单页面URL
  snippet 'nt url_page id' do |cmd|
    cmd.description = '单页面URL'
    cmd.trigger = 'nturlpageid'
    cmd.expansion = '{\$site_host}/index.php?g=portal&m=page&a=index&id=$0'
    cmd.needApplyReContentAssist = true 
  end



  #文章URL_大U
  snippet 'nt url_article U' do |cmd|
    cmd.description = '文章URL 大U'
    cmd.trigger = 'nturlarticleu'
    cmd.expansion = '{:leuu(\'article/index\',array(\'id\'=>\$vo[\'tid\']))}'
    cmd.needApplyReContentAssist = true 
  end
  
 
  #文章URL_大U 指定id
  snippet 'nt url_article U' do |cmd|
    cmd.description = '文章URL 大U id'
    cmd.trigger = 'nturlarticleuid'
    cmd.expansion = '{:leuu(\'article/index\',array(\'id\'=>$1))}'
    cmd.needApplyReContentAssist = true 
  end
  
   
  #文章URL
  snippet 'nt url_article' do |cmd|
    cmd.description = '文章URL'
    cmd.trigger = 'nturlarticle'
    cmd.expansion = '{\$site_host}/index.php?g=portal&m=article&a=index&id=$1&cid=$2'
    cmd.needApplyReContentAssist = true 
  end
  
############################### URL/end #########################################


############################### title/start #####################################
#头部 - 首页
  snippet 'nt title_index' do |cmd|
    cmd.description = '首页头部'
    cmd.trigger = 'nttitle_index'
    cmd.expansion = '<title>{\$site_seo_title} {\$site_name}</title>
<meta name="keywords" content="{\$site_seo_keywords}" />
<meta name="description" content="{\$site_seo_description}" />'
    cmd.needApplyReContentAssist = true 
  end

#头部 - 列表
  snippet 'nt title_list' do |cmd|
    cmd.description = '列表头部'
    cmd.trigger = 'nttitle_list'
    cmd.expansion = '<title>{\$name} {\$seo_title} {\$site_name}</title>
<meta name="keywords" content="{\$seo_keywords}" />
<meta name="description" content="{\$seo_description}" />'
    cmd.needApplyReContentAssist = true 
  end


#头部 - 内容页
  snippet 'nt title_article' do |cmd|
    cmd.description = '内容页头部'
    cmd.trigger = 'nttitle_article'
    cmd.expansion = '<title>{\$post_title} {\$site_name} </title>
<meta name="keywords" content="{\$post_keywords}" />
<meta name="description" content="{\$post_excerpt}" />'
    cmd.needApplyReContentAssist = true 
  end


#头部 - 单页面
  snippet 'nt title_page' do |cmd|
    cmd.description = '单页面头部'
    cmd.trigger = 'nttitle_page'
    cmd.expansion = '<title>{\$post_title} {\$site_name} </title>
<meta name="keywords" content="{\$post_keywords}" />
<meta name="description" content="{\$post_excerpt}" />'
    cmd.needApplyReContentAssist = true 
end
  
############################### title/end #####################################


############################### pos/start #####################################
# 所在位置 - 列表页
snippet 'nt pos_list' do |cmd|
    cmd.description = '所在位置 - 列表页'
    cmd.trigger = 'ntpos_list'
    cmd.expansion = '{:sp_get_path_list(\$cat_id)}'
    cmd.needApplyReContentAssist = true 
end

# 所在位置 - 内容页
snippet 'nt pos_article' do |cmd|
    cmd.description = '所在位置 - 内容页'
    cmd.trigger = 'ntpos_article'
    cmd.expansion = '{:sp_get_path_list(\$term[\'term_id\'], \$post_title)}'
    cmd.needApplyReContentAssist = true 
end

# 所在位置 - 单页面
snippet 'nt pos_page' do |cmd|
    cmd.description = '所在位置 - 单页面'
    cmd.trigger = 'ntpos_page'
    cmd.expansion = '{:sp_get_path_page(\$id)}'
    cmd.needApplyReContentAssist = true 
end


############################### pos/end #####################################


############################### extend/start #####################################
#设为首页
snippet 'nt SetHome' do |s|
  s.trigger = 'ntsethome'
  s.description = '设为首页'
  s.expansion = "<a href=\"#\" onclick=\"SetHome(this,\'{\\$site_host}\')\">设为首页</a>"
end

#加入收藏
snippet 'nt AddFavorite' do |s|
  s.trigger = 'ntaddfavorite'
  s.description = '加入收藏'
  s.expansion = "<a href=\"#\" onclick=\"AddFavorite('{\\$site_host}','{\\$site_name}')\">加入收藏</a>"
end

############################### extend/end #####################################


############################### search/start #####################################

#搜索表单
snippet 'nt SearchForm' do |s|
  s.trigger = 'ntsearchform'
  s.description = '搜索表单'
  s.expansion = "<form method=\"post\" class=\"form-inline\" action=\"{:U('portal/search/index')}\">
                 <input type=\"text\" class=\"\" placeholder=\"请输入搜索名称\" name=\"keyword\" value=\"{:I('get.keyword')}\"/>
                 <input type=\"submit\" value=\"搜索\"/>
            </form>"
end

############################### search/end #####################################


############################### menu/start #####################################

snippet 'nt TermById' do |s|
  s.trigger = 'nttermbyid'
  s.description = '根据指定分类id获得下级的分类'
  s.expansion = "<?php \\$term_id=$1; \\$terms=sp_get_child_terms(\\$term_id ); ?>
<foreach name=\"terms\" item=\"vo\">
    <li><a href=\"{:leuu('list/index',array('id'=>\\$vo['term_id']))}\" target=\"_blank\">{\\$vo.name}</a><\/li>
    <a href=\"{:leuu('list/index',array('id'=>\\$vo['term_id']))}\" target=\"_blank\">{\\$vo.name}</a>
    {\\$vo.termid} {\\$vo.name}
</foreach>  "
end



snippet 'nt TermByIdNum' do |s|
  s.trigger = 'nttermbyidnum'
  s.description = '根据指定分类id获得下级的分类的数量'
  s.expansion = "<?php \\$term_id=$1; \\$terms=sp_get_child_terms_num(\\$term_id ,\\$num); ?>
<foreach name=\"terms\" item=\"vo\">
    <li><a href=\"{:leuu('list/index',array('id'=>\\$vo['term_id']))}\" target=\"_blank\">{\\$vo.name}</a><\/li>
    <a href=\"{:leuu('list/index',array('id'=>\\$vo['term_id']))}\" target=\"_blank\">{\\$vo.name}</a>
    {\\$vo.termid} {\\$vo.name}
</foreach>  "
end
############################### menu/end #######################################


############################### swiper/start ####################################

snippet 'nt swiper' do |s|
  s.trigger = 'ntswiper'
  s.description = 'swiper 幻灯'
  s.expansion = "<link rel=\"stylesheet\" type=\"text/css\" href=\"__TMPL__Public/assets/swiper/swiper-3.3.1.min.css\" />
<style>
/*swiper*/
#swiper{}
    .swiper-container {width: 100%;height: 570px;}
    .swiper-slide {text-align: center;font-size: 18px; background: #fff;}
    .swiper-container .swiper-slide { height: 300px;line-height: 300px;}
    .swiper-container .swiper-slide:nth-child(2n) {height: 500px; line-height: 500px; }
</style>
<php>
\\$home_slides=sp_getslide(\"$1\");
\\$home_slides=empty(\\$home_slides)?\\$default_home_slides:\\$home_slides;
</php>

<!--Swiper/start-->
<div id=\"swiper\">
    <div class=\"swiper-container\">
        <div class=\"swiper-wrapper\">
            <foreach name=\"home_slides\" item=\"vo\">
            <div class=\"swiper-slide\"><a href=\"{\\$vo.slide_url}\"><img src=\"{:sp_get_asset_upload_path(\\$vo['slide_pic'])}\" alt=\"\"></a></div>
            </foreach>
        </div>
        <!-- Add Pagination -->
        <div class=\"swiper-pagination\"></div>
        <!-- Add Arrows -->
        <div class=\"swiper-button-next\"></div>
        <div class=\"swiper-button-prev\"></div>
    </div>
</div>
<!--Swiper/end-->
<!--swipper/start-->
<script src=\"__TMPL__Public/js/jquery.min.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script src=\"__TMPL__Public/assets/swiper/swiper-3.3.1.jquery.min.js\" type=\"text/javascript\" charset=\"utf-8\"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoHeight: false, //enable auto height
        loop:true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
</script>
<!--swipper/end-->"
end

############################### swipper/end ####################################

############################### map/start ######################################

snippet 'nt map' do |s|
  s.trigger = 'ntmap'
  s.scope = '地图'
  s.expansion = "<script src=\"http://api.map.baidu.com/api?v=1.3\"></script>
<div id=\"mapCanvas\" class=\"map-canvas no-margin\" style=\"height: 300px;\"> 
<script type=\"text/javascript\">             
var map = new BMap.Map(\"mapCanvas\"); // 创建Map实例
    var point = new BMap.Point(\"112.588144\", \"37.864518\"); // 创建点坐标
    map.centerAndZoom(point, 15); // 初始化地图,设置中心点坐标和地图级别。
    map.enableScrollWheelZoom(); //启用滚轮放大缩小
    //添加缩放控件
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());

    var marker = new BMap.Marker(point); // 创建标注 
    map.addOverlay(marker); // 将标注添加到地图中
    var infoWindow = new BMap.InfoWindow(\"创泰网络科技<br/><span class=''>地址：太原市迎泽区迎泽南街鼎元时代中心B座803室</span>\"); // 创建信息窗口对象
    marker.openInfoWindow(infoWindow);
</script>"
end

############################### map/end ########################################


############################### admin/start ########################################

#管理员模板头部
snippet 'nt ace header' do |s|
  s.trigger = 'ntaheader'
  s.description = '管理员模板头部'
  s.expansion = "<admintpl file=\"header\" />
</head>
<body>"
end 
  
#管理员模板wrapper
snippet 'nt ace wrapper' do |s|
  s.trigger = 'ntawrapper'
  s.description = '管理员模板wrapper'
  s.expansion = "<div class=\"wrap js-check-wrap\">
  $1
</div>"
end

#管理员模板nav-列表index
snippet 'nt ace nav index' do |s|
  s.trigger = 'ntanavindex'
  s.description = '管理员模板nav-列表index'
  s.expansion = "<ul class=\"nav nav-tabs\">
    <li class=\"active\"><a href=\"{:U('$1/index')}\">$2管理</a></li>
    <li><a href=\"{:U('$3/add')}\">添加$4</a></li>
    
</ul>"
end

#管理员模板nav-填加add
snippet 'nt ace nav add' do |s|
  s.trigger = 'ntanavadd'
  s.description = '管理员模板nav-填加add'
  s.expansion = "<ul class=\"nav nav-tabs\">
    <li><a href=\"{:U('$1/index')}1\">$2管理</a></li>
    <li class=\"active\"><a href=\"{:U('$3/add')}\">添加$4</a></li>
</ul>

"
end

#管理员模板form表单 index列表
snippet 'nt ace form index' do |s|
  s.trigger = 'ntaform'
  s.description = '管理员模板form表单 index列表'
  s.expansion = "<form method=\"post\" class=\"js-ajax-form\">
    <div class=\"table-actions\">
        <button class=\"btn btn-primary btn-small js-ajax-submit\" type=\"submit\" data-action=\"{:U('ad/toggle',array('display'=>1))}\" data-subcheck=\"true\"></button>
        <button class=\"btn btn-primary btn-small js-ajax-submit\" type=\"submit\" data-action=\"{:U('ad/toggle',array('hide'=>1))}\" data-subcheck=\"true\">{:L('HIDE')}</button>
    </div>
    <php> \\$status=array(\"1\"=>L('DISPLAY'),\"0\"=>L('HIDDEN')); </php>
    <table class=\"table table-hover table-bordered table-list\">
        <thead>
            <tr>
                <th width=\"16\"><label><input type=\"checkbox\" class=\"js-check-all\" data-direction=\"x\" data-checklist=\"js-check-x\"></label></th>
                <th width=\"50\">ID</th>
                <th>{:L('NAME')}</th>
                <th>{:L('HOW_TO_USE')}</th>
                <th width=\"45\">{:L('STATUS')}</th>
                <th width=\"120\">{:L('ACTIONS')}</th>
            </tr>
        </thead>
        <tbody>
            <foreach name=\"ads\" item=\"vo\">
            <tr>
                <td><input type=\"checkbox\" class=\"js-check\" data-yid=\"js-check-y\" data-xid=\"js-check-x\" name=\"ids[]\" value=\"{\\$vo.ad_id}\"></td>
                <td>{\\$vo.ad_id}</td>
                <td>{\\$vo.ad_name}</td>
                <php>\\$usercode=\"{\".\":sp_getad('\".\\$vo['ad_name'].\"')}\";</php>
                <td>{\\$usercode}</td>
                <td>{\\$status[\\$vo['status']]}</td>
                <td>
                    <a href=\"{:U('ad/edit',array('id'=>\\$vo['ad_id']))}\">{:L('EDIT')}</a> |
                    <a href=\"{:U('ad/delete',array('id'=>\\$vo['ad_id']))}\" class=\"js-ajax-delete\">{:L('DELETE')}</a>
                </td>
            </tr>
            </foreach>
        </tbody>
        <tfoot>
            <tr>
                <th width=\"16\"><label><input type=\"checkbox\" class=\"js-check-all\" data-direction=\"x\" data-checklist=\"js-check-x\"></label></th>
                <th width=\"50\">ID</th>
                <th>{:L('NAME')}</th>
                <th>{:L('HOW_TO_USE')}</th>
                <th width=\"45\">{:L('STATUS')}</th>
                <th width=\"120\">{:L('ACTIONS')}</th>
            </tr>
        </tfoot>
    </table>
    <div class=\"table-actions\">
        <button class=\"btn btn-primary btn-small js-ajax-submit\" type=\"submit\" data-action=\"{:U('ad/toggle',array('display'=>1))}\" data-subcheck=\"true\">{:L('DISPLAY')}</button>
        <button class=\"btn btn-primary btn-small js-ajax-submit\" type=\"submit\" data-action=\"{:U('ad/toggle',array('hide'=>1))}\" data-subcheck=\"true\">{:L('HIDE')}</button>
    </div>
    <div class=\"pagination\">{\\$Page}</div>
</form>
"
end



snippet 'nt ace form add' do |s|
  s.trigger = 'ntaformadd'
  s.description = '管理员模板表单form-添加add'
  s.expansion = "<form method=\"post\" class=\"form-horizontal js-ajax-form\" action=\"{:U('ad/add_post')}\">
    <fieldset>
        <div class=\"control-group\">
            <label class=\"control-label\">名称</label>
            <div class=\"controls\">
                <input type=\"text\" name=\"ad_name\">
                <span class=\"form-required\">*</span>
            </div>
        </div>
        <div class=\"control-group\">
            <label class=\"control-label\">名称2</label>
            <div class=\"controls\">
                <textarea name=\"ad_content\" rows=\"5\" cols=\"57\"></textarea>
            </div>
        </div>
    </fieldset>
    <div class=\"form-actions\">
        <button type=\"submit\" class=\"btn btn-primary js-ajax-submit\">填加</button>
        <a class=\"btn\" href=\"{:U('ad/index')}\">退回</a>
    </div>
</form>"
end

snippet 'nt ace search' do |s|
  s.trigger = 'ntasearch'
  s.description = '管理员模板搜索表单'
  s.expansion = "<form class=\"well form-search\" method=\"post\" action=\"{:U('Ad/index')}\">
    关键字： 
    <input type=\"text\" name=\"keyword\" style=\"width: 200px;\" value=\"{\\$formget.keyword}\" placeholder=\"请输入关键字...\">
    <input type=\"submit\" class=\"btn btn-primary\" value=\"搜索\" />
</form>"
end

snippet 'nt ace page' do |s|
  s.trigger = 'ntapage'
  s.description = '管理员模板分页'
  s.expansion = "<div class=\"pagination\">{\\$Page}</div>"
end


snippet 'nt ace footer' do |s|
  s.trigger = 'ntafooter'
  s.description = '管理员模板底部'
  s.expansion = "   <script src=\"__PUBLIC__/js/common.js\"></script>
</body>
</html>"
end

snippet 'nt ad' do |s|
  s.trigger = 'ntad'
  s.description = '广告管理 ,比如 {:sp_getad(\'ad2\')}'
  s.expansion = "{:sp_getad('$1')}"
end

############################### admin/end ########################################

end

#ThinkPHP NewThink 模板标签代码块结束